/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao.subtests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.Namespace;
import de.samply.mdr.dal.dto.Record;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.MDRTestSuite;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.sdao.DAOException;

public class RecordEntryDAOTest extends AbstractTest {

    @Test
    public void insertAndFind() throws DAOException {
        MDRConnection mdr = MDRTestSuite.get();
        ElementDAO dao = mdr.get(ElementDAO.class);

        Namespace ns = (Namespace) dao.getNamespaces().get(0);

        Record record = new Record();
        dao.saveElement(record);

        ScopedIdentifier identifier = new ScopedIdentifier();
        identifier.setElementId(record.getId());
        identifier.setIdentifier("3453E");
        identifier.setVersion("1");
        identifier.setStatus(Status.RELEASED);
        identifier.setNamespaceId(ns.getId());
        identifier.setUrl("NONE");
        identifier.setElementType(ElementType.RECORD);
        identifier.setNamespace("mdr");
        identifier.setCreatedBy(1);

        mdr.get(ScopedIdentifierDAO.class).saveScopedIdentifier(identifier);

        Record record2 = (Record) dao.getElement(record.getId());

        assertTrue(record2.equals(record));

        commit(mdr);
    }

}
