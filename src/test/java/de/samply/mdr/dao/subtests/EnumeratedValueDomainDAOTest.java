/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao.subtests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.samply.mdr.dal.dto.DataElement;
import de.samply.mdr.dal.dto.Definition;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.EnumeratedValueDomain;
import de.samply.mdr.dal.dto.PermissibleValue;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Status;
import de.samply.mdr.dao.DefinitionDAO;
import de.samply.mdr.dao.ElementDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.MDRTestSuite;
import de.samply.mdr.dao.ScopedIdentifierDAO;
import de.samply.sdao.DAOException;

public class EnumeratedValueDomainDAOTest extends AbstractTest {

    @Test
    public void insertAndFind() throws DAOException {
        EnumeratedValueDomain domain = new EnumeratedValueDomain();
        domain.setDatatype("char");
        domain.setFormat("c");
        domain.setMaxCharacters(1);
        domain.setUnitOfMeasure("none");

        MDRConnection mdr = MDRTestSuite.get();
        ElementDAO dao = mdr.get(ElementDAO.class);
        DefinitionDAO ddao = mdr.get(DefinitionDAO.class);
        ScopedIdentifierDAO scdao = mdr.get(ScopedIdentifierDAO.class);

        dao.saveElement(domain);
//        assertTrue(domain.equals(dao.getEnumeratedValueDomain(domain.getId())));

        DataElement element = new DataElement();
        element.setValueDomainId(domain.getId());

        dao.saveElement(element);

        ScopedIdentifier identifier = new ScopedIdentifier();
        identifier.setStatus(Status.RELEASED);
        identifier.setVersion("1.0");
        identifier.setUrl("http://none.org");
        identifier.setNamespaceId(dao.getNamespaces().get(0).getId());
        identifier.setElementId(element.getId());
        identifier.setIdentifier("" + element.getId());
        identifier.setElementType(ElementType.DATAELEMENT);
        identifier.setCreatedBy(1);

        scdao.saveScopedIdentifier(identifier);

        Definition elementDef = new Definition();
        elementDef.setDefinition("Das Genotypische Geschlecht des Patienten");
        elementDef.setDesignation("Geschlecht");
        elementDef.setLanguage("de");
        elementDef.setElementId(element.getId());
        elementDef.setScopedIdentifierId(identifier.getId());

        ddao.saveDefinition(elementDef);

        elementDef = new Definition();
        elementDef.setDefinition("The patients sex");
        elementDef.setDesignation("Sex");
        elementDef.setLanguage("en");
        elementDef.setElementId(element.getId());
        elementDef.setScopedIdentifierId(identifier.getId());

        ddao.saveDefinition(elementDef);

        PermissibleValue value = new PermissibleValue();
        value.setValueDomainId(domain.getId());
        value.setPermittedValue("m");

        dao.saveElement(value);
        assertTrue(value.equals(dao.getElement(value.getId())));
        assertTrue(dao.getPermissibleValues(domain.getId()).size() == 1);
        assertTrue(dao.getPermissibleValues(domain.getId()).contains(value));

        Definition definition = new Definition();
        definition.setDefinition("Classifies the patients gender as male");
        definition.setDesignation("Male");
        definition.setLanguage("en");
        definition.setElementId(value.getId());
        definition.setScopedIdentifierId(identifier.getId());

        ddao.saveDefinition(definition);

        definition = new Definition();
        definition.setDefinition("Klassifiziert das Geschlecht des Patienten als genotypisch männlich");
        definition.setDesignation("Männlich");
        definition.setLanguage("de");
        definition.setElementId(value.getId());
        definition.setScopedIdentifierId(identifier.getId());

        ddao.saveDefinition(definition);

        value = new PermissibleValue();
        value.setValueDomainId(domain.getId());
        value.setPermittedValue("f");

        dao.saveElement(value);
        assertTrue(value.equals(dao.getElement(value.getId())));
        assertTrue(dao.getPermissibleValues(domain.getId()).size() == 2);
        assertTrue(dao.getPermissibleValues(domain.getId()).contains(value));

        definition = new Definition();
        definition.setDefinition("Classifies the patients gender as female");
        definition.setDesignation("Female");
        definition.setLanguage("en");
        definition.setElementId(value.getId());
        definition.setScopedIdentifierId(identifier.getId());

        ddao.saveDefinition(definition);

        definition = new Definition();
        definition.setDefinition("Klassifiziert das Geschlecht des Patienten als genotypisch weiblich");
        definition.setDesignation("Weiblich");
        definition.setLanguage("de");
        definition.setElementId(value.getId());
        definition.setScopedIdentifierId(identifier.getId());

        ddao.saveDefinition(definition);

        assertTrue(definition.equals(ddao.getDefinition(definition.getId())));
        assertTrue(ddao.getDefinitions(definition.getElementId(), identifier.getId()).size() == 2);
        assertTrue(ddao.getDefinitions(definition.getElementId(), identifier.getId()).contains(definition));
        assertTrue(ddao.getDefinitions(definition.getElementId(), identifier.getId(), "de").size() == 1);
        assertTrue(ddao.getDefinitions(definition.getElementId(), identifier.getId(), "de").contains(definition));

        commit(mdr);
        mdr.close();
    }
}
