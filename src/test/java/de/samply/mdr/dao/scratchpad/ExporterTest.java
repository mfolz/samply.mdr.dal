package de.samply.mdr.dao.scratchpad;

import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import de.samply.config.util.JAXBUtil;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dao.IdentifiedDAO;
import de.samply.mdr.dao.MDRConnection;
import de.samply.mdr.dao.utils.Exporter;
import de.samply.mdr.xsd.ObjectFactory;
import de.samply.sdao.DAOException;

/**
 *
 */
public class ExporterTest {

    public static void main(String[] args) {
        try (MDRConnection mdr = new MDRConnection("localhost", "mdr", "paul", "", 0)) {
            List<IdentifiedElement> rootElements = mdr.get(IdentifiedDAO.class).getRootElements("registry");

            Exporter exporter = new Exporter(mdr);

            for(IdentifiedElement element : rootElements) {
                exporter.add(element);
            }

//            exporter.add(mdr.get(IdentifiedDAO.class).getElement("urn:test-ns:catalog:1:1"));

            System.out.println(JAXBUtil.marshall(exporter.generateExport(), JAXBContext.newInstance(ObjectFactory.class)));

        } catch (DAOException | JAXBException e) {
            e.printStackTrace();
        }
    }

}
