
ALTER TABLE "element" ALTER COLUMN "uuid" SET NOT NULL;


UPDATE "scopedIdentifier" AS sc2 SET uuid =
    (SELECT uuid FROM "scopedIdentifier" as sc WHERE sc."old_uuid" = sc2."old_uuid" ORDER BY sc."id" LIMIT 1);

ALTER TABLE "scopedIdentifier" DROP COLUMN "old_uuid";

ALTER TABLE "scopedIdentifier" ALTER COLUMN "uuid" SET NOT NULL;


ALTER TABLE "mdrUser" ADD COLUMN "canExportImport" BOOLEAN;

UPDATE "mdrUser" SET "canExportImport" = false;

ALTER TABLE "mdrUser" ALTER COLUMN "canExportImport" SET NOT NULL;
