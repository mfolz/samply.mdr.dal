/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import de.samply.mdr.dal.dto.Definition;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.AlreadySavedException;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;
import de.samply.string.util.StringUtil;
import de.samply.string.util.StringUtil.Builder;

/**
 * DAO that returns Definitions.
 * @author paul
 *
 */
public class DefinitionDAO extends AbstractDAO<Definition> {

    protected DefinitionDAO(ConnectionProvider provider) {
        super(provider);
    }

    @Override
    public Definition getObject(ResultSet set) throws SQLException {
        Definition def = new Definition();
        def.setId(set.getInt(Definition.ID.alias));
        def.setElementId(set.getInt(Definition.ELEMENT_ID.alias));
        def.setScopedIdentifierId(set.getInt(Definition.SCOPED_ID.alias));
        def.setDefinition(set.getString(Definition.DEFINITION.alias));
        def.setDesignation(set.getString(Definition.DESIGNATION.alias));
        def.setLanguage(set.getString(Definition.LANGUAGE.alias));
        return def;
    }

    /**
     * Saves the given definition in the database. Expects a definition linked
     * to an element.
     *
     * @param def
     * @throws DAOException
     */
    public void saveDefinition(Definition def) throws DAOException {
        if(def.getId() != 0) {
            throw new AlreadySavedException();
        }

        def.setId(insert(Definition.TABLE,
                Definition.ELEMENT_ID.val(def.getElementId()),
                Definition.SCOPED_ID.val(def.getScopedIdentifierId()),
                Definition.DESIGNATION.val(def.getDesignation()),
                Definition.DEFINITION.val(def.getDefinition()),
                Definition.LANGUAGE.val(def.getLanguage())));

    }

    /**
     * Saves the given definition in the database. Expects a definition linked
     * to a namespace.
     * @param def
     * @throws DAOException
     */
    public void saveNamespaceDefinition(Definition def) throws DAOException {
        if(def.getId() != 0) {
            throw new AlreadySavedException();
        }
        def.setId(
                insert(Definition.TABLE,
                        Definition.ELEMENT_ID.val(def.getElementId()),
                        Definition.DESIGNATION.val(def.getDesignation()),
                        Definition.DEFINITION.val(def.getDefinition()),
                        Definition.LANGUAGE.val(def.getLanguage())));
    }

    /**
     * Returns all definitions for the given element and scoped identifier.
     * @param elementId
     * @param scopedIdentifierId
     * @return Array of definitions, might be empty.
     * @throws DAOException
     */
    public List<Definition> getDefinitions(int elementId, int scopedIdentifierId) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                + Definition.ELEMENT_ID.access() + " = ? AND "
                + Definition.SCOPED_ID.access() + " = ?",
                elementId, scopedIdentifierId);
    }

    /**
     * Returns all definitions for the given element and scoped identifier for the given languages.
     * @param elementId
     * @param scopedIdentifierId
     * @param languages
     * @return Array of definitions, might be empty.
     * @throws DAOException
     */
    public List<Definition> getDefinitions(int elementId, int scopedIdentifierId, List<String> languages) throws DAOException {
        if(languages == null || languages.size() == 0) {
            return getDefinitions(elementId, scopedIdentifierId);
        } else {
            return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE "
                    + Definition.ELEMENT_ID.access() + " = ? AND "
                    + Definition.SCOPED_ID.access() + " = ? AND ("
                    + StringUtil.join(languages, " OR ", new Builder<String>() {
                        @Override
                        public String build(String o) {
                            return Definition.LANGUAGE.access() + " = ?";
                        }
                    }) + ")", elementId, scopedIdentifierId, languages);
        }
    }

    /**
     * Returns the definition for the given element and scoped identifier in the specified language.
     * @param elementId
     * @param scopedIdentifierId
     * @param language
     * @return
     * @throws DAOException
     */
    public List<Definition> getDefinitions(int elementId, int scopedIdentifierId, String language) throws DAOException {
        List<String> target = new ArrayList<>();
        target.add(language);
        return getDefinitions(elementId, scopedIdentifierId, target);
    }

    /**
     * Returns the definition with the given ID.
     * @param id
     * @return
     * @throws DAOException
     */
    public Definition getDefinition(int id) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " WHERE " + Definition.ID.access() + " = ?", id);
    }

    @Override
    protected String getInsertTable() {
        throw new UnsupportedOperationException();
    }

    final static String SQL_SELECT_FIELDS = getSelectFields(Definition.class);

    final static String SQL_TABLE = Definition.TABLE.from();

}
