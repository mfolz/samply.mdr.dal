/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import de.samply.mdr.dal.dto.Element;
import de.samply.mdr.dal.dto.ElementType;
import de.samply.mdr.dal.dto.IdentifiedElement;
import de.samply.mdr.dal.dto.Misc;
import de.samply.mdr.dal.dto.Misc.ScopedHierarchy;
import de.samply.mdr.dal.dto.ScopedIdentifier;
import de.samply.mdr.dal.dto.Status;
import de.samply.sdao.AbstractDAO;
import de.samply.sdao.AlreadySavedException;
import de.samply.sdao.ConnectionProvider;
import de.samply.sdao.DAOException;
import de.samply.sdao.ObjectMapper;

/**
 * DAO for scoped identifier.
 */
public class ScopedIdentifierDAO extends AbstractDAO<ScopedIdentifier> {

    protected ScopedIdentifierDAO(ConnectionProvider master) {
        super(master);
    }

    /**
     * Saves the given scoped identifier in the database.
     * @param identifier
     * @throws DAOException
     */
    public void saveScopedIdentifier(ScopedIdentifier identifier) throws DAOException {
        if(identifier.getId() != 0) {
            throw new AlreadySavedException();
        }

        identifier.setId(insert(ScopedIdentifier.TABLE,
                ScopedIdentifier.NAMESPACE_ID.val(identifier.getNamespaceId()),
                ScopedIdentifier.ELEMENT_ID.val(identifier.getElementId()),
                ScopedIdentifier.VERSION.val(identifier.getVersion()),
                ScopedIdentifier.STATUS.val(identifier.getStatus()),
                ScopedIdentifier.IDENTIFIER.val(identifier.getIdentifier()),
                ScopedIdentifier.URL.val(identifier.getUrl()),
                ScopedIdentifier.ELEMENT_TYPE.val(identifier.getElementType()),
                ScopedIdentifier.CREATED_BY.val(identifier.getCreatedBy()),
                ScopedIdentifier.UUID.val(identifier.getUuid() == null ? UUID.randomUUID() : identifier.getUuid())));
    }

    /**
     * Returns the scoped identifier with the given ID.
     * @param id
     * @return
     * @throws DAOException
     */
    public ScopedIdentifier getScopedIdentifier(int id) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE
                + " INNER JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " WHERE " + ScopedIdentifier.ID.access() + " = ?", id);
    }

    /**
     * Returns the scoped identifier with the given UUID in the given namespace.
     * @param uuid
     * @return
     * @throws DAOException
     */
    public ScopedIdentifier getScopedIdentifier(UUID uuid) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE
                + " INNER JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " WHERE " + ScopedIdentifier.UUID.access() + " = ?::uuid", uuid.toString());
    }

    /**
     * Returns the scoped identifier with the given UUID in the given namespace.
     * @param uuid
     * @return
     * @throws DAOException
     */
    public ScopedIdentifier getScopedIdentifier(UUID uuid, String namespace) throws DAOException {
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE
                + " INNER JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " WHERE " + ScopedIdentifier.UUID.access() + " = ?::uuid"
                + " AND " + Misc.NAME.access() + " = ?", uuid.toString(), namespace);
    }

    /**
     * Adds the given scoped identifier (subId) as subordinate to the first scoped identifier (superId).
     * @param superId
     * @param subId
     * @param order
     * @throws DAOException
     */
    public void addSubIdentifier(int superId, int subId) throws DAOException {
        insert(ScopedHierarchy.TABLE,
                ScopedHierarchy.SUB.val(subId),
                ScopedHierarchy.SUPER.val(superId));
    }

    /**
     * Adds the given scoped identifier (subId) as subordinate to the first scoped identifier (superId) in
     * the given order.
     * @param superId
     * @param subId
     * @param order
     * @throws DAOException
     */
    public void addSubIdentifier(int superId, int subId, int order) throws DAOException {
        insert(ScopedHierarchy.TABLE,
                ScopedHierarchy.SUB.val(subId),
                ScopedHierarchy.SUPER.val(superId),
                ScopedHierarchy.ORDER.val(order));
    }

    /**
     * Returns a free identifier for the given namespace and element type.
     * @param namespace
     * @param type
     * @return
     * @throws DAOException
     */
    public String getFreeIdentifier(String namespace, ElementType type) throws DAOException {
        List<String> max = executeSelect("SELECT MAX(CASE WHEN " + ScopedIdentifier.IDENTIFIER.access() + " ~ E'^\\\\d+$' THEN "
                    + ScopedIdentifier.IDENTIFIER.access() + "::int ELSE '0' END) FROM " + SQL_TABLE + " LEFT JOIN "
                    + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access() + " WHERE "
                    + Misc.NAME.access() + " = ? AND " + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\"", new ObjectMapper<String>() {
                    @Override
                    public String getObject(ResultSet set) throws SQLException {
                        return set.getString("max");
                    }
            }, namespace, type);
        if(max.size() > 0) {
            return "" + (Integer.parseInt(max.get(0)) + 1);
        } else {
            return "1";
        }
    }

    /**
     * Returns a new revision for the given URN, meaning the new revision is not used yet.
     * @param urn
     * @return
     * @throws DAOException
     */
    public int getNewRevision(String urn) throws DAOException {
        ScopedIdentifier scoped = ScopedIdentifier.fromURN(urn);

        String result = executeSelect("SELECT "
                + "MAX(CASE WHEN " + ScopedIdentifier.VERSION.access() + " ~ E'^\\\\d+$' THEN "
                    + ScopedIdentifier.VERSION.access() + "::int ELSE '0' END) "
                + "AS \"max\" FROM " + SQL_TABLE
                + " LEFT JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " WHERE " + ScopedIdentifier.IDENTIFIER.access() + " = ? "
                    + "AND " + Misc.NAME.access() + " = ? "
                    + "AND " + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\"", new ObjectMapper<String>() {
                    @Override
                    public String getObject(ResultSet set)
                            throws SQLException {
                        return set.getString("max");
                    }
        }, scoped.getIdentifier(), scoped.getNamespace(), scoped.getElementType()).get(0);

        return Integer.parseInt(result) + 1;
    }

    public List<ScopedIdentifier> getScopedIdentifierForElement(UUID elementUUID, String namespace) throws DAOException {
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + ScopedIdentifierDAO.SQL_TABLE + " INNER JOIN "
                + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " LEFT JOIN " + Element.TABLE.from() + " ON " + Element.ID.access() + " = " + ScopedIdentifier.ELEMENT_ID.access()
                + " WHERE " + Misc.NAME.access() + " = ? AND "
                + Element.UUID.access() + " = ?::uuid ", namespace, elementUUID.toString());
    }

    /**
     * Returns the scoped identifier with the specified URN.
     * @param urn
     * @param namespace
     * @param userId
     * @return
     * @throws DAOException
     */
    public ScopedIdentifier getScopedIdentifierForElement(String urn, String namespace, int userId) throws DAOException {
        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);
        List<ScopedIdentifier> list = executeSelect("SELECT " + ScopedIdentifierDAO.SQL_SELECT_FIELDS + " FROM "
                + ScopedIdentifierDAO.SQL_TABLE + " INNER JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " AND " + ScopedIdentifier.ELEMENT_ID.access() + " = (SELECT " + ScopedIdentifier.ELEMENT_ID.access()
                    + " FROM " + ScopedIdentifierDAO.SQL_TABLE + " INNER JOIN " + Misc.NS.from() + " ON "
                    + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access() + " WHERE "
                        + ScopedIdentifier.IDENTIFIER.access() + " = ? AND "
                        + ScopedIdentifier.VERSION.access() + " = ? AND "
                        + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" AND "
                        + Misc.NAME.access() + " = ?) AND "
                + Misc.NAME.access() + " = ? AND " + ScopedIdentifier.STATUS.access() + " = 'RELEASED' AND " + IdentifiedDAO.SQL_ACCESS_CLAUSE,
                  identifier.getIdentifier(), identifier.getVersion(), identifier.getElementType(),
                  identifier.getNamespace(), namespace, userId);

        if(list.size() == 0) {
            return null;
        } else {
            return list.get(0);
        }
    }

    /**
     * Returns all scoped identifiers that are identical to the scoped identifier with the specified URN.
     * @param urn
     * @param userId
     * @return
     * @throws DAOException
     */
    public List<ScopedIdentifier> findIdenticalScopedIdentifiers(String urn, int userId) throws DAOException {
        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);
        return executeSelect("SELECT " + ScopedIdentifierDAO.SQL_SELECT_FIELDS + " FROM "
                + ScopedIdentifierDAO.SQL_TABLE + " INNER JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " WHERE " + ScopedIdentifier.UUID.access() + " = "
                        + "(SELECT " + ScopedIdentifier.UUID.access() + " FROM " + ScopedIdentifierDAO.SQL_TABLE + " INNER JOIN "
                            + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access() + " "
                            + "WHERE " + ScopedIdentifier.IDENTIFIER.access() + " = ? "
                            + "AND " + ScopedIdentifier.VERSION.access() + " = ? "
                            + "AND " + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" "
                            + "AND " + Misc.NAME.access() + " = ?) "
                + "AND " + IdentifiedDAO.SQL_ACCESS_CLAUSE,
                  identifier.getIdentifier(), identifier.getVersion(), identifier.getElementType(),
                  identifier.getNamespace(), userId);
    }

    /**
     * Returns the scoped identifier in a specific namespace, that is identical to the scoped identifier with the given URN.
     * @param urn
     * @param namespace
     * @param userId
     * @return
     * @throws DAOException
     */
    public ScopedIdentifier getIdenticalScopedIdentifier(String urn, String namespace, int userId) throws DAOException {
        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);
        List<ScopedIdentifier> list = executeSelect("SELECT " + ScopedIdentifierDAO.SQL_SELECT_FIELDS + " FROM "
                + ScopedIdentifierDAO.SQL_TABLE + " INNER JOIN " + Misc.NS.from() + " ON "
                + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " AND " + ScopedIdentifier.UUID.access() + " = (SELECT " + ScopedIdentifier.UUID.access() + " FROM "
                    + ScopedIdentifierDAO.SQL_TABLE + " INNER JOIN " + Misc.NS.from()
                    + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access() + " WHERE "
                        + ScopedIdentifier.IDENTIFIER.access() + " = ? AND "
                        + ScopedIdentifier.VERSION.access() + " = ? AND "
                        + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" AND "
                        + Misc.NAME.access() + " = ?) AND "
                + Misc.NAME.access() + " = ? AND " + IdentifiedDAO.SQL_ACCESS_CLAUSE,
                  identifier.getIdentifier(), identifier.getVersion(), identifier.getElementType(),
                  identifier.getNamespace(), namespace, userId);

        if(list.size() == 0) {
            return null;
        } else {
            return list.get(0);
        }
    }

    /**
     * Returns the specified scoped identifier.
     * @param urn
     * @return
     * @throws DAOException
     */
    public ScopedIdentifier getScopedIdentifier(String urn) throws DAOException {
        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);
        List<ScopedIdentifier> list = executeSelect("SELECT " + ScopedIdentifierDAO.SQL_SELECT_FIELDS + " FROM " + ScopedIdentifierDAO.SQL_TABLE
                + " INNER JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access() + " WHERE "
                + ScopedIdentifier.IDENTIFIER.access() + " = ? AND "
                + ScopedIdentifier.VERSION.access() + " = ? AND "
                + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" AND "
                + Misc.NAME.access() + " = ? AND " + IdentifiedDAO.SQL_ACCESS_CLAUSE,
                identifier.getIdentifier(), identifier.getVersion(),
                identifier.getElementType(), identifier.getNamespace(), getUserId());

        if(list.size() == 0) {
            return null;
        } else {
            return list.get(0);
        }
    }

    /**
     * Returns the latest released Scoped Identifier for the specified URN.
     * @param urn
     * @return
     * @throws DAOException
     */
    public ScopedIdentifier getLatestVersion(String urn) throws DAOException {
        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);
        return executeSingleSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " INNER JOIN "
                + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " WHERE " + ScopedIdentifier.IDENTIFIER.access() + " = ? AND "
                + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" AND "
                + Misc.NAME.access() + " = ? AND "
                + ScopedIdentifier.VERSION.access() + " = ("
                    + "SELECT MAX(" + ScopedIdentifier.VERSION.access() + ") FROM " + SQL_TABLE + " INNER JOIN " + Misc.NS.from()
                    + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access() + " WHERE "
                        + ScopedIdentifier.IDENTIFIER.access() + " = ? AND "
                        + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" AND "
                        + Misc.NAME.access() + " = ? AND "
                        + ScopedIdentifier.STATUS.access() + " = 'RELEASED')", identifier.getIdentifier(),
                    identifier.getElementType(), identifier.getNamespace(), identifier.getIdentifier(),
                    identifier.getElementType(), identifier.getNamespace());
    }

    /**
     * Returns a list of all available versions for the given URN.
     * @param urn
     * @return
     * @throws DAOException
     */
    public List<ScopedIdentifier> getVersions(String urn) throws DAOException {
        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);
        return executeSelect("SELECT " + SQL_SELECT_FIELDS + " FROM " + SQL_TABLE + " INNER JOIN "
                + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + " WHERE " + ScopedIdentifier.IDENTIFIER.access() + " = ? "
                + "AND " + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" "
                + "AND " + Misc.NAME.access() + " = ?", identifier.getIdentifier(), identifier.getElementType(),
                identifier.getNamespace());
    }

    /**
     * Outdates the scoped identifier with the given URN.
     * @param urn
     * @throws DAOException
     */
    public void outdateIdentifier(String urn) throws DAOException {
        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);
        executeUpdate("UPDATE " + ScopedIdentifier.TABLE.table() + " SET " + ScopedIdentifier.STATUS.column() + " = 'OUTDATED' "
                + "WHERE " + ScopedIdentifier.ID.column() + " IN (SELECT " + ScopedIdentifier.ID.access() + " FROM "
                + SQL_TABLE + " LEFT JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                + "WHERE " + ScopedIdentifier.IDENTIFIER.access() + " = ? "
                + "AND " + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" "
                + "AND " + Misc.NAME.access() + " = ? "
                + "AND " + ScopedIdentifier.VERSION.access() + " = ?)",
                identifier.getIdentifier(), identifier.getElementType(), identifier.getNamespace(), identifier.getVersion());

        if(identifier.getElementType() == ElementType.CATALOG) {
            List<IdentifiedElement> allSubMembers = provider.get(IdentifiedDAO.class).getAllSubMembers(identifier.toString());
            for(IdentifiedElement element : allSubMembers) {
                outdateIdentifier(element.getScoped().toString());
            }
        }
    }

    /**
     * Outdates all scoped identifier with the given URN except the version.
     * Also outdates all codes, if the URN describes a catalog.
     * @param urn
     * @throws DAOException
     */
    public void outdateIdentifiers(String urn) throws DAOException {
        ScopedIdentifier identifier = ScopedIdentifier.fromURN(urn);
        executeUpdate("UPDATE " + ScopedIdentifier.TABLE.table() + " SET " + ScopedIdentifier.STATUS.column() + " = 'OUTDATED' "
                + "WHERE " + ScopedIdentifier.STATUS.column() + " = 'RELEASED' "
                + "AND " + ScopedIdentifier.ID.column() + " IN (SELECT " + ScopedIdentifier.ID.access() + " FROM " + SQL_TABLE
                    + " LEFT JOIN " + Misc.NS.from() + " ON " + Misc.ID.access() + " = " + ScopedIdentifier.NAMESPACE_ID.access()
                    + " WHERE " + ScopedIdentifier.IDENTIFIER.access() + " = ? "
                    + "AND " + ScopedIdentifier.ELEMENT_TYPE.access() + " = ?::\"elementType\" "
                    + "AND " + Misc.NAME.access() + " = ?)",
                identifier.getIdentifier(), identifier.getElementType(), identifier.getNamespace());

        if(identifier.getElementType() == ElementType.CATALOG) {
            List<IdentifiedElement> allSubMembers = provider.get(IdentifiedDAO.class).getAllSubMembers(identifier.toString());
            for(IdentifiedElement element : allSubMembers) {
                outdateIdentifiers(element.getScoped().toString());
            }
        }
    }

    /**
     * Releases the Scoped identifier with the given ID.
     * @param id
     * @throws DAOException
     */
    public void release(int id) throws DAOException {
        update(ScopedIdentifier.TABLE, ScopedIdentifier.ID.val(id),
                ScopedIdentifier.STATUS.val(Status.RELEASED));
    }

    /**
     * Releases the given scoped identifier and all codes, if the scoped identifier describes a catalog.
     * @param scopedIdentifier
     * @throws DAOException
     */
    public void release(ScopedIdentifier scopedIdentifier) throws DAOException {
        release(scopedIdentifier.getId());

        if(scopedIdentifier.getElementType() == ElementType.CATALOG) {
            List<IdentifiedElement> allSubMembers = provider.get(IdentifiedDAO.class).getAllSubMembers(scopedIdentifier.toString());
            for(IdentifiedElement element : allSubMembers) {
                release(element.getScoped().getId());
            }
        }
    }

    @Override
    public ScopedIdentifier getObject(ResultSet set) throws SQLException {
        ScopedIdentifier identifier = new ScopedIdentifier();
        identifier.setId(set.getInt(ScopedIdentifier.ID.alias));
        identifier.setStatus(Status.valueOf(set.getString(ScopedIdentifier.STATUS.alias)));
        identifier.setNamespaceId(set.getInt(ScopedIdentifier.NAMESPACE_ID.alias));
        identifier.setUrl(set.getString(ScopedIdentifier.URL.alias));
        identifier.setVersion(set.getString(ScopedIdentifier.VERSION.alias));
        identifier.setIdentifier(set.getString(ScopedIdentifier.IDENTIFIER.alias));
        identifier.setElementId(set.getInt(ScopedIdentifier.ELEMENT_ID.alias));
        identifier.setElementType(ElementType.valueOf(set.getString(ScopedIdentifier.ELEMENT_TYPE.alias).toUpperCase()));
        identifier.setNamespace(set.getString(ScopedIdentifier.NAMESPACE.alias));
        identifier.setCreatedBy(set.getInt(ScopedIdentifier.CREATED_BY.alias));
        identifier.setUuid(UUID.fromString(set.getString(ScopedIdentifier.UUID.alias)));
        return identifier;
    }

    @Override
    protected String getInsertTable() {
        throw new UnsupportedOperationException();
    }

    static final String SQL_SELECT_FIELDS = getSelectFields(ScopedIdentifier.class);

    static final String SQL_TABLE = ScopedIdentifier.TABLE.from();

}
