/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dal.dto;

import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;

public class Misc {

    public static final Table NS = new Table("element", "ns");
    public static final Column ID = new Column(NS, "id");
    public static final Column NAME = new Column(NS, "name");
    public static final Column HIDDEN = new Column(NS, "hidden");

    public static class ScopedHierarchy {
        public static final Table TABLE = new Table("scopedIdentifierHierarchy", "h");
        public static final Column SUPER = new Column(TABLE, "superId");
        public static final Column SUB = new Column(TABLE, "subId");
        public static final Column ORDER = new Column(TABLE, "order");
    }

    public static class UserReadableNamespace {
        public static final Table TABLE = new Table("userReadableNamespace", "np");
        public static final Column USER_ID = new Column(TABLE, "userId");
        public static final Column NAMESPACE_ID = new Column(TABLE, "namespaceId");
    }

    public static class UserWritableNamespace {
        public static final Table TABLE = new Table("userWritableNamespace", "np");
        public static final Column USER_ID = new Column(TABLE, "userId");
        public static final Column NAMESPACE_ID = new Column(TABLE, "namespaceId");
    }

}
