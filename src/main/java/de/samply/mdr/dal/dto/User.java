/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dal.dto;

import java.io.Serializable;

import de.samply.mdr.dao.Vocabulary;
import de.samply.sdao.definition.Column;
import de.samply.sdao.definition.Table;
import de.samply.sdao.json.JSONResource;
import de.samply.string.util.StringUtil;

/**
 *
 */
public class User implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public static final Table TABLE = new Table("mdrUser", "mu");

    /**
     * The id in the database
     */
    private int id;
    public static final Column ID = new Column(TABLE, "id");

    /**
     * the username, usually the samply auth subject
     */
    private String username;
    public static final Column USERNAME = new Column(TABLE, "username");

    /**
     * If true, the user can create new namespaces
     */
    private Boolean canCreateNamespace;
    public static final Column CREATE_NAMESPACE = new Column(TABLE, "canCreateNamespace");

    /**
     * If true, the user can upload new catalogs
     */
    private Boolean canCreateCatalog;
    public static final Column CREATE_CATALOG = new Column(TABLE, "canCreateCatalog");

    /**
     * If true, the user can use the export and import
     */
    private Boolean canExportImport;
    public static final Column EXPORT_IMPORT = new Column(TABLE, "canExportImport");

    /**
     * The data column contains various informations about the user, e.g. his real name or
     * the label of the external identity provider.
     */
    private JSONResource data = new JSONResource();
    public static final Column DATA = new Column(TABLE, "data");

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the canCreateNamespace
     */
    public Boolean getCanCreateNamespace() {
        return canCreateNamespace;
    }

    /**
     * @param canCreateNamespace the canCreateNamespace to set
     */
    public void setCanCreateNamespace(Boolean canCreateNamespace) {
        this.canCreateNamespace = canCreateNamespace;
    }

    /**
     * @return the data
     */
    public JSONResource getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(JSONResource data) {
        this.data = data;
    }

    /**
     * Returns the users email address
     * @return
     */
    public String getEmail() {
        if(this.data.getProperty(Vocabulary.EMAIL) != null) {
            return this.data.getProperty(Vocabulary.EMAIL).getValue();
        } else {
            return null;
        }
    }

    /**
     * Sets the users email address
     * @param email
     */
    public void setEmail(String email) {
        this.data.setProperty(Vocabulary.EMAIL, email);
    }

    /**
     * Returns the users real name
     * @return
     */
    public String getRealName() {
        if(this.data.getProperty(Vocabulary.REALNAME) != null) {
            return this.data.getProperty(Vocabulary.REALNAME).getValue();
        } else {
            return null;
        }
    }

    /**
     * Sets the users real name.
     * @param realName
     */
    public void setRealName(String realName) {
        this.data.setProperty(Vocabulary.REALNAME, realName);
    }

    /**
     * Returns the label for the external identity provider.
     * @return
     */
    public String getExternalLabel() {
        if(data.getProperty(Vocabulary.EXTERNAL_LABEL) != null) {
            return data.getProperty(Vocabulary.EXTERNAL_LABEL).getValue();
        } else {
            return null;
        }
    }

    /**
     * Sets the label for the external identity provider.
     * @param label
     */
    public void setExternalLabel(String label) {
        if(StringUtil.isEmpty(label)) {
            data.removeProperties(Vocabulary.EXTERNAL_LABEL);
        } else {
            data.setProperty(Vocabulary.EXTERNAL_LABEL, label);
        }
    }

    /**
     * @return the canCreateCatalog
     */
    public Boolean getCanCreateCatalog() {
        return canCreateCatalog;
    }

    /**
     * @param canCreateCatalog the canCreateCatalog to set
     */
    public void setCanCreateCatalog(Boolean canCreateCatalog) {
        this.canCreateCatalog = canCreateCatalog;
    }

    /**
     * @return the canExportImport
     */
    public Boolean getCanExportImport() {
        return canExportImport;
    }

    /**
     * @param canExportImport the canExportImport to set
     */
    public void setCanExportImport(Boolean canExportImport) {
        this.canExportImport = canExportImport;
    }

}
