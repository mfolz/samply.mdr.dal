/**
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Contact: info@osse-register.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.samply.mdr.dal.dto;

import de.samply.sdao.definition.Column;

/**
 * A described value domain can be used for validation with a regular expression,
 * an integer within a range, etc.
 */
public class DescribedValueDomain extends ValueDomain {

    /**
     *
     */
    private static final long serialVersionUID = 3436937687231210778L;

    private String description;
    public static final Column DESCRIPTION = new Column(TABLE, "description");

    /**
     *
     */
    private ValidationType validationType;
    public static final Column VALIDATION_TYPE = new Column(TABLE, "validationType");

    /**
     * The validationData depends on the validation type:
     *
     *
     */
    private String validationData;
    public static final Column VALIDATION_DATA = new Column(TABLE, "validationData");

    public DescribedValueDomain() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof DescribedValueDomain)) {
            return false;
        }
        DescribedValueDomain ds = (DescribedValueDomain) obj;
        return super.equals(obj) && description.equals(ds.description);
    }

    public String getValidationData() {
        return validationData;
    }

    public void setValidationData(String validationData) {
        this.validationData = validationData;
    }

    public ValidationType getValidationType() {
        return validationType;
    }

    public void setValidationType(ValidationType validationType) {
        this.validationType = validationType;
    }

}
